pyTasklist
==========

Converts yaml files into scheduled tasks, using https://github.com/dbader/schedule by Daniel Bader - `@dbader_org <https://twitter.com/dbader_org>`_ - mail@dbader.org

