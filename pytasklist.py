from __future__ import print_function

import glob
import signal
import subprocess
import sys
import time

import yaml

import schedule


# noinspection PyClassHasNoInit
class bColours:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def interrupt_handler(sig, frame):
    sys.exit(0)


def errormsg(message):
    """
    Display error message with colour code
    :param message:
    """
    print("%s%s%s" % (bColours.FAIL, message, bColours.ENDC))


class Tasklist(object):
    def __init__(self, path):
        """
        Generates a list of Tasks from .yml|.yaml files in path
        :param path: path containing task definition files
        """
        self.tasks = []
        self.path = path
        for f in glob.glob("%s/*.yml" % self.path) + glob.glob("%s/*.yaml" % self.path):
            self.tasks.append(Task(f))


class Task(object):
    def __init__(self, f):
        """
        Generate a task from .yml file f
        :param f: filename of .yml task definition
        """
        self.file = f
        self._y = yaml.load(file(self.file, 'r'))
        self.actions = []
        for action in self._y.get('actions'):
            self.actions.append(action)

    @property
    def interval(self):
        """
        Number of periods to wait between task executions
        :return: int
        """
        return int(self._y.get('interval', 1))

    @property
    def at(self):
        """
        Time of day to execute task, only required if scale is one day or longer
        :return: string
        """
        at = self._y.get('at')
        if at is None:
            raise SyntaxError(
                'Time at which to run task is required for scales of one day or greater.\nPlease specify an "at" field in your configuration.')
        return str(at)

    @property
    def scale(self):
        """
        Scale of intervals to wait between task executions.
        Allows any of the scales defined by the schedule module
        :return: string
        """
        scale = self._y.get('scale', 'days')
        if scale in ['second', 'hour', 'day', 'week'] and self.interval > 1:
            errormsg("WARNING: Detected singular scale (%s), but interval is greater than 1.\nPlease use plural scale such as %ss or set interval to 1." % (scale, scale))
        return scale

    def execute(self):
        """
        Run all shell commands defined in this task
        """
        length = str(len(self.actions))
        for index, action in enumerate(self.actions):
            print("%s %s [%s/%s] :: %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), self.file, index + 1, length, action))
            try:
                subprocess.check_call(action, shell=True)
            except Exception as taskerror:  # catch *all* exceptions
                errormsg("%s" % taskerror)


if __name__ == '__main__':
    if len(sys.argv) == 2:
        configpath = sys.argv[1]
    else:
        configpath = './tasks'

    print("Reading tasks from %s" % configpath)

    for job in Tasklist(configpath).tasks:
        print("\n%s" % job.file)

        try:
            every = schedule.every(job.interval)
            everyinterval = getattr(every, job.scale)

            if job.scale not in ['seconds', 'seconds', 'minute', 'minutes', 'hour', 'hours']:
                print("* every %s %s at %s, tasks:\n  - %s\n" % (
                    job.interval, job.scale, job.at, "\n  - ".join(job.actions)))
                everyinterval.at(job.at).do(job.execute)
            else:
                print("* every %s %s, tasks:\n  - %s\n" % (job.interval, job.scale, "\n  - ".join(job.actions)))
                everyinterval.do(job.execute)
            # all good, continue next task
            continue

        except AssertionError as e:
            errormsg("Mismatch between scale: '%s' and interval: '%s'" % (job.scale, job.interval))
            errormsg("Ignoring task file %s" % job.file)
            schedule.cancel_job(every)
            continue
        except AttributeError as e:
            errormsg("Invalid scale: %s" % job.scale)
            errormsg("Ignoring task file %s" % job.file)
            schedule.cancel_job(every)
            continue
        except TypeError as e:
            errormsg(str(e))
        except SyntaxError as e:
            errormsg(str(e))

        errormsg("Ignoring task file %s" % job.file)
        # if we got here there was an error, so cancel the pending task
        if 'everyinterval' in vars():
            schedule.cancel_job(everyinterval)

    if len(schedule.jobs):
        signal.signal(signal.SIGINT, interrupt_handler)  # quieter keyboard interrupts
        while True:
            schedule.run_pending()
            time.sleep(1)
    else:
        errormsg("\nNo jobs scheduled, exiting")
        exit(1)
